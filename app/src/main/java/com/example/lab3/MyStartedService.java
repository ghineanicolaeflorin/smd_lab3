package com.example.lab3;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.Random;

public class MyStartedService extends Service {

    private static final String TAG = "tag";

    @Override
    public int onStartCommand (Intent intent,int flags, int startId) {
        Log.d(TAG, "Started service " + "Thread used:"+Thread.currentThread());
        randomMessage();
        return START_NOT_STICKY;
    }

    public static Intent getIntent(Context context){
        Intent intent = new Intent(context, MyStartedService.class);
        return intent;
    }

    private void randomMessage(){
        Toast toast = Toast.makeText(MyStartedService.this,"Bits Saved:"+ new Random().nextInt(1000),Toast.LENGTH_SHORT);
        toast.show();
    }

    @Override
    public void onCreate () {
        Log.d(TAG, "Created service " + "Thread used:"+ Thread.currentThread());
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Service Destroyed " + "Thread used:"+ Thread.currentThread());
        super.onDestroy();
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Service Binded " + "Thread used:"+ Thread.currentThread());
        return null;
    }


}
