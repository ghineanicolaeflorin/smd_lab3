package com.example.lab3;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

public class MyBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        int money=intent.getIntExtra("sendBroadcast",0);
        Toast.makeText(context,"You won " + money +" fake euros",Toast.LENGTH_SHORT).show();
    }
}
