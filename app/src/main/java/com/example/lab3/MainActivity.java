package com.example.lab3;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.IntentService;
import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import static android.os.Build.VERSION_CODES.O;

public class MainActivity extends AppCompatActivity {

    private MyBoundService myService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button button_start = findViewById(R.id.button_start);
        Button button_stop = findViewById(R.id.button_stop);
        Button button_bind = findViewById(R.id.button_bind);

        button_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startMyService();
            }
        });

        button_stop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopMyService();
            }
        });

        button_bind.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Toast.makeText(MainActivity.this,MyBoundService.getCurrentDate(),Toast.LENGTH_SHORT).show();
            }
        });
        bindToService();


        //Intent service = new Intent(MainActivity.this,LuckyIntentService.class);
        //startService(service); -problema aici, descoperita dupa push
        //MyBroadcastReceiver broadcastReceiver = new MyBroadcastReceiver();
        //IntentFilter broadcast = new IntentFilter("broadcastReceiver");
        //LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(broadcastReceiver,broadcast);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        unBindToService();
    }



    private void startMyService() {
        Intent start = MyStartedService.getIntent(this);

        if (isOreoOrHigher()) {
            startForegroundService(start);
        } else {
            startService(start);
        }

    }

    private Boolean isOreoOrHigher() {
        return Build.VERSION.SDK_INT >= O;
    }

    private void stopMyService() {
        Intent stop = MyStartedService.getIntent(this);
        stopService(stop);
    }

    private void bindToService(){
        if (myService == null) {
            Intent bind = new Intent(this, MyBoundService.class);
            bindService(bind, serviceConnection, BIND_AUTO_CREATE);
        }
    }

    private void unBindToService(){
        unbindService(serviceConnection);
        }

        public ServiceConnection serviceConnection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
                myService=((MyBoundService.MyBoundServiceBinder) service).get();

            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
                myService=null;
            }
        };
    }