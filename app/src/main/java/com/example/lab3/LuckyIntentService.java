package com.example.lab3;

import android.app.IntentService;
import android.content.Intent;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import java.util.concurrent.ThreadLocalRandom;

public class LuckyIntentService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public LuckyIntentService(String name) {
        super(name);
    }

    @Override
    protected void onHandleIntent(Intent intent){
        if(intent != null) {
            int random_money = ThreadLocalRandom.current().nextInt(0, 100);
            Intent money = new Intent("sendBroadcast");
            money.putExtra("money",random_money);
            LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(money);
        }
    }
}
