package com.example.lab3;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class MyBoundService extends Service {

    private static final String TAG ="tag";

    @Override
    public void onCreate () {
        Log.d(TAG, "Created service " + "Thread used:"+ Thread.currentThread());
        super.onCreate();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "Service Destroyed " + "Thread used:"+ Thread.currentThread());
        super.onDestroy();
    }

    @Override
    public boolean onUnbind(Intent intent){
        Log.d(TAG, "Service Unbinded " + "Thread used:"+ Thread.currentThread());
        return false;
    }

    public static String getCurrentDate(){
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat date = new SimpleDateFormat("dd-MM-yyyy");
        return date.format(c);
    }

    class MyBoundServiceBinder extends Binder{
        private MyBoundService myService = MyBoundService.this;
        public MyBoundService get(){
            return myService;
        }
    }

    private MyBoundServiceBinder myBinder=new MyBoundServiceBinder();

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return myBinder;
    }
}
